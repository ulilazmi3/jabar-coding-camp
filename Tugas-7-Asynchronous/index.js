// soal 1

// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Tulis code untuk memanggil function readBooks di sini
function read(time, books, index) {
    if (index < books.length) {
        readBooks(time, books[index], function(sisaWaktu){
            if (sisaWaktu > 0) {
                index += 1;
                read(sisaWaktu, books, index)
            }
        })
    }
}

read(10000, books, 0) //.... jawaban soal 1
