// soal 1

var nilai = 95;

if ( nilai >= 85 ) {
    console.log("indeksnya A") // .... jawaban soal 1
} else if ( nilai >= 75 && nilai < 85 ) {
    console.log("indeksnya B") // .... jawaban soal 1
} else if ( nilai >= 65 && nilai < 75 ) {
    console.log("indeksnya C") // .... jawaban soal 1
} else if ( nilai >= 55 && nilai < 65 ) {
    console.log("indeksnya D") // .... jawaban soal 1
} else if ( nilai < 55 ) {
    console.log("indeksnya E") // .... jawaban soal 1
}

// soal 2
var tanggal = 22;
var bulan = 7;
var tahun = 2020;

var tanggal = 3;
var bulan = 8;
var tahun = 1999;

switch(bulan) {
    case 1:   { var nama_bulan = 'Januari'; break; }
    case 2:   { var nama_bulan = 'Februari'; break; }
    case 3:   { var nama_bulan = 'Maret'; break; }
    case 4:   { var nama_bulan = 'April'; break; }
    case 5:   { var nama_bulan = 'Mei'; break; }
    case 6:   { var nama_bulan = 'Juni'; break; }
    case 7:   { var nama_bulan = 'Juli'; break; }
    case 8:   { var nama_bulan = 'Agustus'; break; }
    case 9:   { var nama_bulan = 'September'; break; }
    case 10:   { var nama_bulan = 'Oktober'; break; }
    case 11:   { var nama_bulan = 'November'; break; }
    case 12:   { var nama_bulan = 'Desember'; break; }
    default:  { var nama_bulan = 'bulan tidak diketahui'; break; }
}

console.log(tanggal + ' ' + nama_bulan + ' ' + tahun) // .... jawaban soal 2

// soal 3

var n = 7;
var output = '';
for (i = 1; i <= n; i++) {
    for (j = 1; j <= i; j++) {
        output += '#';
    }
    console.log(output); // .... jawaban soal 3
    output = '';
}

// soal 4

var m = 10
var A = 'I love programming'
var B = 'I love Javascript'
var C = 'I love VueJS' 
var pembatas = '';

for (i = 1; i <= m; i++){
    if(i % 3 == 1){
        console.log(i + ' - ' + A) // .... jawaban soal 4
        pembatas += '=';
    } else if(i % 3 == 2){
        console.log(i + ' - ' + B) // .... jawaban soal 4
        pembatas += '=';
    } else if(i % 3 == 0){
        console.log(i + ' - ' + C) // .... jawaban soal 4
        pembatas += '=';
        console.log(pembatas) // .... jawaban soal 4
    }
}
