//1. Function Penghitung Jumlah Kata
function jumlah_kata(string) {
    return string.trim().split(" ").length;
  }
  

var kalimat_1 = " Halo nama saya Muhammad Iqbal Mubarok "
var kalimat_2 = "Saya Iqbal"
  
  
console.log(jumlah_kata(kalimat_1)) // 6 //Jawaban soal 1
console.log(jumlah_kata(kalimat_2)) // 2 //Jawaban soal 2

//2. Judul : Function Penghasil Tanggal Hari Esok

function kabisat(tahun){
    if (tahun % 400 == 0){
        return true
    }
    else if (tahun % 100 == 0){
        return false
    } 
    else if (tahun % 4 == 0){
        return true
    }
    else {return false}
}

function next_date(tanggal, bulan, tahun){
    tanggal += 1
    if( (tanggal>31) && (bulan == 1 || bulan == 3 || bulan == 5 || bulan == 7 || bulan == 8 || bulan == 10) ){
        tanggal -= 31;
        bulan += 1;
    }
    else if( (tanggal>30) && (bulan == 4 || bulan == 6 || bulan == 9 || bulan == 11)){
        tanggal -= 30;
        bulan += 1;
    }
    else if(tanggal > 29 && kabisat(tahun) && bulan == 2){
        tanggal -= 29;
        bulan += 1;
    }
    else if(tanggal > 28 && (kabisat(tahun) == false) && bulan == 2){
        tanggal -= 28;
        bulan += 1;
    }
    else if(tanggal > 31 && bulan == 12){
        tanggal -= 31;
        bulan -= 11;
        tahun += 1;
    }

    switch(bulan) {
        case 1:   { var nama_bulan = 'Januari'; break; }
        case 2:   { var nama_bulan = 'Februari'; break; }
        case 3:   { var nama_bulan = 'Maret'; break; }
        case 4:   { var nama_bulan = 'April'; break; }
        case 5:   { var nama_bulan = 'Mei'; break; }
        case 6:   { var nama_bulan = 'Juni'; break; }
        case 7:   { var nama_bulan = 'Juli'; break; }
        case 8:   { var nama_bulan = 'Agustus'; break; }
        case 9:   { var nama_bulan = 'September'; break; }
        case 10:   { var nama_bulan = 'Oktober'; break; }
        case 11:   { var nama_bulan = 'November'; break; }
        case 12:   { var nama_bulan = 'Desember'; break; }
        default:  { var nama_bulan = 'bulan tidak diketahui'; break; }
    }

    console.log(tanggal + ' ' + nama_bulan + ' ' + tahun) // .... jawaban soal 2
}

//contoh 1

var tanggal = 29
var bulan = 2
var tahun = 2020

next_date(tanggal , bulan , tahun ) // output : 1 Maret 2020 // .... jawaban soal 2

// contoh 2

var tanggal = 28
var bulan = 2
var tahun = 2021

next_date(tanggal , bulan , tahun ) // output : 1 Maret 2021 // .... jawaban soal 2

// contoh 3

var tanggal = 31
var bulan = 12
var tahun = 2020

next_date(tanggal , bulan , tahun ) // output : 1 Januari 2021 // .... jawaban soal 2