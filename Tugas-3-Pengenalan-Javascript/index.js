// soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

console.log(pertama.substr(0, 4).concat(pertama.substr(11, 8)).concat(kedua.substr(0, 8)).concat(kedua.substr(8, 10).toUpperCase())); // .... jawaban soal 1

// soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var a1 = parseInt(kataPertama)
var a2 = parseInt(kataKedua)
var a3 = parseInt(kataKetiga)
var a4 = parseInt(kataKeempat)

console.log((a1%a2) + (a3*a4)) // .... jawaban soal 2


// soal 3

//buatlah variabel-variabel seperti di bawah ini

var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); // do your own! 
var kataKetiga = kalimat.substring(15, 18); // do your own! 
var kataKeempat = kalimat.substring(19, 24); // do your own! 
var kataKelima = kalimat.substring(25, 31); // do your own! 

console.log('Kata Pertama: ' + kataPertama); // .... jawaban soal 3
console.log('Kata Kedua: ' + kataKedua); // .... jawaban soal 3
console.log('Kata Ketiga: ' + kataKetiga); // .... jawaban soal 3
console.log('Kata Keempat: ' + kataKeempat); // .... jawaban soal 3
console.log('Kata Kelima: ' + kataKelima); // .... jawaban soal 3