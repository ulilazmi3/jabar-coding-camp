//Soal 1

const  luas_dan_keliling_persegi_panjang = (sisi) => {
    //function
    const jumlah_sisi = 4
    let keliling = jumlah_sisi * sisi
    let luas = sisi * sisi
    return console.log(`Luas dan keliling persegi panjang adalah : ${luas} dan ${keliling}`)
} //Jawaban Soal 1

let sisi = 10

luas_dan_keliling_persegi_panjang(sisi)


//Soal 2

// Fungsi lama
// const newFunction = function literal(firstName, lastName){
//     return {
//       firstName: firstName,
//       lastName: lastName,
//       fullName: function(){
//         console.log(firstName + " " + lastName)
//       }
//     }
//   }

//Fungsi Baru
const newFunction = literal = (firstName, lastName) => {
    return {
      firstName,
      lastName,
      fullName(){
        console.log(firstName + " " + lastName)
      }
    }
  } //Jawaban Soal 2
   
//Driver Code 
newFunction("William", "Imoh").fullName() 

// Soal 3

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
  }

const {firstName, lastName, address, hobby} = newObject //Jawaban Soal 3

// Driver code
console.log(firstName, lastName, address, hobby)

//Soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
let combined = [...west, ...east] //Jawaban Soal 4
//Driver Code
console.log(combined)

//Soal 5

const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 
let after = `Lorem ${view}dolor sit amet, consectetur adipiscing elit,${planet}` //Jawaban Soal 5

console.log(before)
console.log(after)
