// soal 1

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

daftarHewan.sort()

daftarHewan.forEach(function(item){
    console.log(item) // .... jawaban soal 1
 })


// soal 2

function introduce(data) {
    return "Nama saya " + data.name + ", umur saya " + data.age + " tahun, alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.hobby + "!" // .... jawaban soal 2
  }
   
 
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
 
var perkenalan = introduce(data)
console.log(perkenalan) // Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan Pelesiran, dan saya punya hobby yaitu Gaming!"  // .... jawaban soal 2

// soal 3
function hitung_huruf_vokal(input) {
    var jumlah_huruf_vokal = 0
    var input_1 = input.toLowerCase()
    var item_array = input_1.split("")
    item_array.forEach(function(item){
        if (item == "a" || item == "i" || item == "u" || item == "e" || item == "o") {
            jumlah_huruf_vokal += 1;
        }
     })
     
    return jumlah_huruf_vokal
  }

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) // 3 2 // .... jawaban soal 3

// soal 4
function hitung(input) {
    return input * 2 - 2
  }

console.log( hitung(0) ) // -2 // .... jawaban soal 4
console.log( hitung(1) ) // 0 // .... jawaban soal 4
console.log( hitung(2) ) // 2 // .... jawaban soal 4
console.log( hitung(3) ) // 4 // .... jawaban soal 4
console.log( hitung(5) ) // 8 // .... jawaban soal 4

// soal 5
// .... jawaban soal 5